package com.abonifacino.turinstops.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.abonifacino.turinstops.model.Departure;
import com.abonifacino.turinstops.model.Stop;

@Service
public class GttStopsHttpClientService {	
	private static final Logger log = LoggerFactory.getLogger(GttStopsHttpClientService.class);
		
	@Value("${gtt.service.get.departure.url:http://gpa.madbob.org/query.php?stop={stop}}")
	private String getDepartureUrl;
	@Value("${gtt.service.get.stops.url:http://www.5t.torino.it/ws2.1/rest/stops/all}")
	private String getStopsUrl;
	
	/**
	 * @author abonifacino
	 * @param stop
	 * @return the list of <strong>departures</strong> of a given <strong>stop</strong>.
	 */
	public List<Departure> getGttDeparture(String stop) {
		String url = this.getDepartureUrl.replace("{stop}", stop);

		HttpEntity<String> entity = getBaseHttpEntity();

		ResponseEntity<List<Departure>> response = null;
		RestTemplate restTemplate = new RestTemplate();
		response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<Departure>>() {
        });

		if (response == null) {
			log.error("response is empty");
			// TODO throw exception
			return null;
		}
		HttpStatus httpStatus = response.getStatusCode();
		if (!httpStatus.is2xxSuccessful()) {
			log.error("httpStatus is equals to {}", httpStatus);
			// TODO throw exception
			return null;
		}
		if (!response.hasBody()) {
			log.error("response has not a body");
			// TODO throw exception
			return null;
		}
		
		return response.getBody();
	}

	/**
	 * @author abonifacino
	 * @return all gtt <strong>stops</strong>
	 */
	public List<Stop> getGttStops(){
		String url = this.getStopsUrl;
		HttpEntity<String> entity = getBaseHttpEntity();

		ResponseEntity<HashMap<String,List<Stop>>> response = null;
		RestTemplate restTemplate = new RestTemplate();
		response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<HashMap<String,List<Stop>>>() {
        });

		if (response == null) {
			log.error("response is empty");
			// TODO throw exception
			return null;
		}
		HttpStatus httpStatus = response.getStatusCode();
		if (!httpStatus.is2xxSuccessful()) {
			log.error("httpStatus is equals to {}", httpStatus);
			// TODO throw exception
			return null;
		}
		if (!response.hasBody()) {
			log.error("response has not a body");
			// TODO throw exception
			return null;
		}
		
		return response.getBody().get("stops");
	}
	private HttpEntity<String> getBaseHttpEntity() {
		HttpEntity<String> entity;
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypeList);
		entity = new HttpEntity<String>(headers);
		return entity;
	}
}
