package com.abonifacino.turinstops.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abonifacino.turinstops.model.Departure;
import com.abonifacino.turinstops.model.Stop;
import com.abonifacino.turinstops.repository.StopRepository;

@Service
public class StopService {
	private static final Logger log = LoggerFactory.getLogger(StopService.class);
	
	@Autowired
	private GttStopsHttpClientService gttStopsHttpClientService; 
	@Autowired
	private StopRepository stopRepository;
	
	public List<Departure> getGttDeparture(String stop) {		
		return gttStopsHttpClientService.getGttDeparture(stop);
	}
	
	public List<Stop> getGttStops(){
		return (List<Stop>) stopRepository.findAll();
	}
	
	public void updateGttStops() {
		List<Stop> stops = gttStopsHttpClientService.getGttStops();
		if (stops == null || stops.size()==0) {
			log.error("No stops were found");
			return;
		}
		stopRepository.deleteAll();
		for (Stop stop: stops) {
			stopRepository.save(stop);
		}
	}
}
