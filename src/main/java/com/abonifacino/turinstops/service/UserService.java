package com.abonifacino.turinstops.service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abonifacino.turinstops.model.User;
import com.abonifacino.turinstops.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * @author abonifacino
	 * @param authorization
	 * @return the authorized <strong>user</strong> it exists, <strong>null</strong> otherwise. 
	 * @throws IllegalArgumentException
	 */
	public User userBasicAuthentication(String authorization) throws IllegalArgumentException{
		User user;
		final String[] values;
		// Authorization: Basic base64credentials
		String base64Credentials = authorization.substring("Basic".length()).trim();
		byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
		String credentials = new String(credDecoded, StandardCharsets.UTF_8);
		// credentials = username:password
		values = credentials.split(":", 2);
		user = userRepository.findByEmail(values[0]);
		user = (user != null && user.getPassword().equals(values[1])) ? user : null;
		return user;
	}
}
