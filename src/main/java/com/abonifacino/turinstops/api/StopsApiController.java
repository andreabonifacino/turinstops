package com.abonifacino.turinstops.api;

import com.abonifacino.turinstops.model.Departure;
import com.abonifacino.turinstops.model.Stop;
import com.abonifacino.turinstops.model.User;
import com.abonifacino.turinstops.service.StopService;
import com.abonifacino.turinstops.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
public class StopsApiController implements StopsApi {

    private static final Logger log = LoggerFactory.getLogger(StopsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public StopsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }
    
	@Autowired
	private UserService userService;
	
	@Autowired
	private StopService stopService;

	private User user;

    public ResponseEntity<List<Departure>>gttDepartures(@ApiParam(value = "The name of the stop.",required=true) @PathVariable("stop") String stop,@ApiParam(value = "" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization) {
    	log.info("Begin gttDepartures. Stop: ".concat(stop));
    	String accept = request.getHeader("Accept");
    	if (accept != null && accept.contains("application/json") && authorization != null
				&& authorization.toLowerCase().startsWith("basic")) {			
			try {
				user = userService.userBasicAuthentication(authorization);
			} catch (IllegalArgumentException e) {
				log.error("Error during base64Credentials decoding: ".concat(e.getMessage()));
				return new ResponseEntity<List<Departure>>(HttpStatus.BAD_REQUEST);
			}			
			if (user != null) {
				List<Departure> departures = stopService.getGttDeparture(stop);
				if (departures != null) {
					return new ResponseEntity<List<Departure>>(departures,HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<List<Departure>>(HttpStatus.UNAUTHORIZED);
			}
		}
    	log.info("End gttDepartures. Stop: ".concat(stop));
		return new ResponseEntity<List<Departure>>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<List<Stop>> gttStops(@ApiParam(value = "" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization) {
        log.info("Begin gttStops. Stop: ");
    	String accept = request.getHeader("Accept");
    	if (accept != null && accept.contains("application/json") && authorization != null
				&& authorization.toLowerCase().startsWith("basic")) {			
			try {
				user = userService.userBasicAuthentication(authorization);
			} catch (IllegalArgumentException e) {
				log.error("Error during base64Credentials decoding: ".concat(e.getMessage()));
				return new ResponseEntity<List<Stop>>(HttpStatus.BAD_REQUEST);
			}			
			if (user != null) {
				List<Stop> stops = stopService.getGttStops();
				if (stops != null) {
					return new ResponseEntity<List<Stop>>(stops,HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<List<Stop>>(HttpStatus.UNAUTHORIZED);
			}
		}
    	log.info("End gttStops. Stop: ");
		return new ResponseEntity<List<Stop>>(HttpStatus.BAD_REQUEST);
    }

}
