package com.abonifacino.turinstops.api;

import com.abonifacino.turinstops.model.User;
import com.abonifacino.turinstops.service.UserService;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import javax.servlet.http.HttpServletRequest;

@Controller
public class UserApiController implements UserApi {

	private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public UserApiController(HttpServletRequest request) {
		this.request = request;
	}

	@Autowired
	private UserService userService;

	private User user;

	public ResponseEntity<User> loginUser(
			@ApiParam(value = "", required = true) @RequestHeader(value = "Authorization", required = true) String authorization) {
		log.info("Begin loginUser");
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json") && authorization != null
				&& authorization.toLowerCase().startsWith("basic")) {			
			try {
				user = userService.userBasicAuthentication(authorization);
			} catch (IllegalArgumentException e) {
				log.error("Error during base64Credentials decoding: ".concat(e.getMessage()));
				return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
			}			
			if (user != null) {
				return new ResponseEntity<User>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<User>(user, HttpStatus.UNAUTHORIZED);
			}
		}
		log.info("End loginUser");
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

}
