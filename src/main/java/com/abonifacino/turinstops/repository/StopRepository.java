package com.abonifacino.turinstops.repository;

import org.springframework.data.repository.CrudRepository;

import com.abonifacino.turinstops.model.Stop;

public interface StopRepository extends CrudRepository<Stop, Integer> {

}
