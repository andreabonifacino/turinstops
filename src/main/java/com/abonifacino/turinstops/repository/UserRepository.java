package com.abonifacino.turinstops.repository;

import org.springframework.data.repository.CrudRepository;
import com.abonifacino.turinstops.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	User findByEmail(String email);
}