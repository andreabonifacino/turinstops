package com.abonifacino.turinstops.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * Departure
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-03-09T10:34:59.316Z")

public class Departure   {
  @JsonProperty("line")
  private String line = null;

  @JsonProperty("hour")
  private String hour = null;

  @JsonProperty("realtime")
  private Boolean realtime = null;

  public Departure line(String line) {
    this.line = line;
    return this;
  }

  /**
   * Get line
   * @return line
  **/
  @ApiModelProperty(value = "")


  public String getLine() {
    return line;
  }

  public void setLine(String line) {
    this.line = line;
  }

  public Departure hour(String hour) {
    this.hour = hour;
    return this;
  }

  /**
   * Get hour
   * @return hour
  **/
  @ApiModelProperty(value = "")


  public String getHour() {
    return hour;
  }

  public void setHour(String hour) {
    this.hour = hour;
  }

  public Departure realtime(Boolean realtime) {
    this.realtime = realtime;
    return this;
  }

  /**
   * Get realtime
   * @return realtime
  **/
  @ApiModelProperty(value = "")


  public Boolean isRealtime() {
    return realtime;
  }

  public void setRealtime(Boolean realtime) {
    this.realtime = realtime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Departure departure = (Departure) o;
    return Objects.equals(this.line, departure.line) &&
        Objects.equals(this.hour, departure.hour) &&
        Objects.equals(this.realtime, departure.realtime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(line, hour, realtime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Departure {\n");
    
    sb.append("    line: ").append(toIndentedString(line)).append("\n");
    sb.append("    hour: ").append(toIndentedString(hour)).append("\n");
    sb.append("    realtime: ").append(toIndentedString(realtime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

