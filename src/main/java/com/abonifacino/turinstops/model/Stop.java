package com.abonifacino.turinstops.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Stop
 */
@Validated

@Entity
public class Stop {
	@Id	
	private String id = null;

	@JsonProperty("lat")
	private String lat = null;

	@JsonProperty("lng")
	private String lng = null;

	@JsonProperty("lines")
	@Lob
	@Column
	private String stopLines = null;

	@JsonProperty("location")
	private String location = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("placeName")
	private String placeName = null;

	@JsonProperty("type")
	private String type = null;

	public Stop id(String id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Stop lat(String lat) {
		this.lat = lat;
		return this;
	}

	/**
	 * Get lat
	 * 
	 * @return lat
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public Stop lng(String lng) {
		this.lng = lng;
		return this;
	}

	/**
	 * Get lng
	 * 
	 * @return lng
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public Stop lines(String lines) {
		this.stopLines = lines;
		return this;
	}

	/**
	 * Get lines
	 * 
	 * @return lines
	 **/
	@ApiModelProperty(value = "")

	public String getLines() {
		return stopLines;
	}

	public void setLines(String lines) {
		this.stopLines = lines;
	}

	public Stop location(String location) {
		this.location = location;
		return this;
	}

	/**
	 * Get location
	 * 
	 * @return location
	 **/
	@ApiModelProperty(value = "")

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Stop name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "")

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Stop placeName(String placeName) {
		this.placeName = placeName;
		return this;
	}

	/**
	 * Get placeName
	 * 
	 * @return placeName
	 **/
	@ApiModelProperty(value = "")

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public Stop type(String type) {
		this.type = type;
		return this;
	}

	/**
	 * Get type
	 * 
	 * @return type
	 **/
	@ApiModelProperty(value = "")

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Stop stop = (Stop) o;
		return Objects.equals(this.id, stop.id) && Objects.equals(this.lat, stop.lat)
				&& Objects.equals(this.lng, stop.lng) && Objects.equals(this.stopLines, stop.stopLines)
				&& Objects.equals(this.location, stop.location) && Objects.equals(this.name, stop.name)
				&& Objects.equals(this.placeName, stop.placeName) && Objects.equals(this.type, stop.type);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, lat, lng, stopLines, location, name, placeName, type);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Stop {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    lat: ").append(toIndentedString(lat)).append("\n");
		sb.append("    lng: ").append(toIndentedString(lng)).append("\n");
		sb.append("    lines: ").append(toIndentedString(stopLines)).append("\n");
		sb.append("    location: ").append(toIndentedString(location)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    placeName: ").append(toIndentedString(placeName)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
