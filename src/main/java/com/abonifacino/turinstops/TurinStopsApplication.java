package com.abonifacino.turinstops;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.abonifacino.turinstops.model.User;
import com.abonifacino.turinstops.repository.UserRepository;
import com.abonifacino.turinstops.service.StopService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "com.abonifacino.turinstops", "com.abonifacino.turinstops.api" , "com.abonifacino.turinstops.configuration"})
public class TurinStopsApplication implements CommandLineRunner {
	@Autowired 
	private UserRepository userRepository;
	@Autowired 
	private StopService stopService;
	
    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
        
        createDefaultUser();
        stopService.updateGttStops();        
    }

    /**
     * creates the default user
     * @author abonifacino
     */
	private void createDefaultUser() {
		// Creation default user
        User u = new User();
        u.setEmail("email@example.it");
        u.setFirstName("firstName");
        u.setLastName("lastName");
        u.setPassword("password");
        
        if (userRepository.findByEmail(u.getEmail()) == null) {
        	userRepository.save(u);
        }
	}

    public static void main(String[] args) throws Exception {
        new SpringApplication(TurinStopsApplication.class).run(args);
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }
    }
}
