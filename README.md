# Turin Stops - Ennova Selection

Candidate: Andrea Bonifacino


### Dependencies  
1. maven
2. docker
3. docker-compose
4. git (optional)

### Installation ###
Run the follow commands:

```
#!bash
cd <to_your_workspace_folder>
git clone https://andreabonifacino@bitbucket.org/andreabonifacino/turinstops-frontend.git
cd turinstops-frontend
docker build -t turin-stops-frontend .
cd <to_your_workspace_folder>
git clone https://andreabonifacino@bitbucket.org/andreabonifacino/turinstops.git
cd turinstops
mvn install
```

### Run project ###
Run the follow commands:

```
#!bash
docker-compose up -d
```

api docs: http://localhost:1234/swagger-ui.html#

front-end: http://localhost:8000

Test User: email@example.it password

### For test: from the browser use a CORS plugin to allow cross origin requests ###